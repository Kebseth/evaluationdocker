FROM maven:3.6.3-jdk-13 as build
WORKDIR /src
COPY pom.xml pom.xml
RUN mvn dependency:go-offline
COPY . .
RUN mvn package

FROM openjdk:13.0-slim
COPY --from=build src/target/simple-http-hello-world-1.0-SNAPSHOT-jar-with-dependencies.jar .
EXPOSE 8181

CMD java -jar *.jar